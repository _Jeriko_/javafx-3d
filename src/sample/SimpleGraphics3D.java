package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.*;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.*;
import javafx.scene.text.Font;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Transform;
import javafx.stage.Stage;

public class SimpleGraphics3D extends Application {
    private static final int WIDTH=800;
    private static final int HEIGHT=500;
    private static final int BUTTON_WIDTH=300;
    private static final int BUTTON_HEIGHT=100;

    private Shape3D shape;
    private SmartGroup group;
    @Override
    public void start(Stage primaryStage) throws Exception{

        Button boxButton = new Button("Box");
        Button sphereButton = new Button("Sphere");
        Button cylinderButton = new Button("Cylinder");

        Font f = new Font(25);

        Image backImage = new Image("/images/mainBack.jpg");
        BackgroundImage backgroundImage = new BackgroundImage(backImage,BackgroundRepeat.ROUND,BackgroundRepeat.ROUND,BackgroundPosition.DEFAULT,BackgroundSize.DEFAULT);
        Background mainBack = new Background(backgroundImage);

        /*Image boxButtonImage = new Image("/images/purpleGradient.jpg");
        BackgroundImage bti = new BackgroundImage(boxButtonImage,BackgroundRepeat.ROUND,BackgroundRepeat.ROUND,BackgroundPosition.DEFAULT,BackgroundSize.DEFAULT);
        Background boxButtonBackground = new Background(bti);

        Image sphereButtonImage = new Image("/images/redGradient.jpg");
        BackgroundImage sbi = new BackgroundImage(sphereButtonImage,BackgroundRepeat.ROUND,BackgroundRepeat.ROUND,BackgroundPosition.CENTER,BackgroundSize.DEFAULT);
        Background sphereButtonBackground = new Background(sbi);

        Image cylinderButtonImage = new Image("/images/greenGradient.jpg");
        BackgroundImage cbi = new BackgroundImage(cylinderButtonImage,BackgroundRepeat.ROUND,BackgroundRepeat.ROUND,BackgroundPosition.CENTER,BackgroundSize.DEFAULT);
        Background cylinderButtonBackground = new Background(cbi);*/

        //sphereButton.setPadding(new Insets(0,25,0,25));
        boxButton.setPrefSize(BUTTON_WIDTH,BUTTON_HEIGHT);
        boxButton.setFont(f);
        boxButton.setBackground(null);

        sphereButton.setPrefSize(BUTTON_WIDTH,BUTTON_HEIGHT);
        sphereButton.setFont(f);
        sphereButton.setBackground(null);

        //cylinderButton.setPadding(new Insets(0,25,0,25));
        cylinderButton.setPrefSize(BUTTON_WIDTH,BUTTON_HEIGHT);
        cylinderButton.setFont(f);
        cylinderButton.setBackground(null);

        VBox vBox = new VBox(boxButton,sphereButton,cylinderButton);
        vBox.setTranslateX((WIDTH/2) - 150);
        //vBox.setPadding(new Insets(0,100,0,100));

        boxButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                setProp(primaryStage,new Box(100,100,100));
            }
        });

        sphereButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                setProp(primaryStage,new Sphere(50));
            }
        });

        cylinderButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                setProp(primaryStage,new Cylinder(50,100));
            }
        });

        Pane startPane = new Pane(vBox);
        startPane.setBackground(mainBack);

        Scene scene = new Scene(startPane,WIDTH,HEIGHT,true);
        scene.setFill(Color.GRAY);

        primaryStage.setScene(scene);
        primaryStage.setTitle("3D");
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
    private void setProp(Stage primaryStage,Shape3D shape){
        group = new SmartGroup();
        group.setTranslateX(WIDTH/2);
        group.setTranslateY(HEIGHT/2);
        group.setTranslateZ(-5);

        PointLight light = new PointLight();
        light.setTranslateX(350);
        light.setTranslateY(500);
        light.setTranslateZ(500);

        group.getChildren().addAll(shape,light);

        Camera camera = new PerspectiveCamera(true);

        camera.translateXProperty().set(WIDTH/2);
        camera.translateYProperty().set(HEIGHT/2);
        camera.translateZProperty().set(-500);

        camera.nearClipProperty().set(1);
        camera.farClipProperty().set(10000);

        Scene scene = new Scene(group,WIDTH,HEIGHT,true);
        scene.setFill(Color.GRAY);
        scene.setCamera(camera);
        primaryStage.setScene(scene);

        PhongMaterial material = new PhongMaterial();
        shape.setMaterial(material);

        primaryStage.addEventHandler(KeyEvent.KEY_PRESSED,event->{
            switch (event.getCode()){
                case UP:
                    group.setTranslateZ(group.getTranslateZ()+100);
                    break;
                case DOWN:
                    group.setTranslateZ(group.getTranslateZ()-100);
                    break;
                case LEFT:
                    group.rotateByZ(-10);
                    break;
                case RIGHT:
                    group.rotateByZ(10);
                    break;
                case DIGIT1:
                    material.setDiffuseColor(Color.RED);
                    break;
                case DIGIT2:
                    material.setDiffuseColor(Color.GREEN);
                    break;
                case DIGIT3:
                    material.setDiffuseColor(Color.YELLOW);
                    break;
                case DIGIT4:
                    material.setDiffuseColor(Color.BLUE);
                    break;
                case DIGIT5:
                    material.setDiffuseColor(Color.VIOLET);
                    break;
                case DIGIT6:
                    material.setDiffuseColor(Color.BROWN);
                    break;
                case DIGIT7:
                    material.setDiffuseColor(Color.WHITE);
                    break;
                case DIGIT8:
                    material.setDiffuseColor(Color.ORANGE);
                    break;
                case DIGIT9:
                    material.setDiffuseColor(Color.GRAY);
                    break;
                case DIGIT0:
                    material.setDiffuseColor(Color.BLACK);
                    break;
                case NUMPAD8:
                    group.rotateByX(10);
                    break;
                case NUMPAD2:
                    group.rotateByX(-10);
                    break;
                case NUMPAD4:
                    group.rotateByY(-10);
                    break;
                case NUMPAD6:
                    group.rotateByY(10);
                    break;
            }
        });
        primaryStage.setScene(scene);
    }
    class SmartGroup extends Group {
        private Rotate r;
        private Transform t = new Rotate();

        public void rotateByX(int ang) {
            r = new Rotate(ang,Rotate.X_AXIS);
            t = t.createConcatenation(r);
            this.getTransforms().clear();
            this.getTransforms().addAll(t);
        }
        public void rotateByY(int ang) {
            r = new Rotate(ang,Rotate.Y_AXIS);
            t = t.createConcatenation(r);
            this.getTransforms().clear();
            this.getTransforms().addAll(t);
        }
        public void rotateByZ(int ang) {
            r = new Rotate(ang,Rotate.Z_AXIS);
            t = t.createConcatenation(r);
            this.getTransforms().clear();
            this.getTransforms().addAll(t);
        }
    }
}
